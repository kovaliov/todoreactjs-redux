import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

import Header from './components/header.component';
import Todo from './components/todo.component';
import Form from './components/form.component';

import todoApp from './reducers';

import Actions from './actions';

import { createStore, applyMiddleware, compose } from 'redux';
import { Provider, connect } from 'react-redux';
import thunk from 'redux-thunk';

import Guid from 'guid';

const guid = function ({ getState }) {
  return (next) => (action) => {
    const payload = action.payload;
    const isPayloadArray = Array.isArray(payload);
    if (isPayloadArray) {
      payload.forEach((value) => {
        const hasGuid = !!value.guid;

        if (!hasGuid) {
          value.guid = Guid.raw();
        }
      });
    } else {
      const hasGuid = !!payload.guid;
      console.log(payload, hasGuid);
      if (!hasGuid) {
        payload.guid = Guid.raw();
      }
    }
    console.log(action);

    return next(action);
  };
};

const store = createStore(
  todoApp,
  compose(
    applyMiddleware(thunk, guid),
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
  )
);

window.store = store;

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      todos: []
    };

    this.props.onInitialTodos();

    this.handleStatusChange = this.handleStatusChange.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
    this.handleAdd = this.handleAdd.bind(this);
    this.handleEdit = this.handleEdit.bind(this);
  }

  handleStatusChange(id) {
    this.props.onPatchTodo(id);
  }

  handleDelete(id) {
    this.props.onDeleteTodo(id);
  }

  handleAdd(title) {
    this.props.onAddTodo({ title }, this.handleError);
  }

  handleEdit(id, title) {
    this.props.onUpdateTodo(id, { title });
  }

  handleError(err) {
    console.log(err);
  }

  nextId() {
    this._nextId = this._nextId || 4;

    return this._nextId++;
  }

  componentWillReceiveProps(nextProps) {
    const todos = nextProps.todos;

    this.setState({
      todos
    });
  }

  render() {
    return (
      <div className="main">

        <Header title={this.props.title} todos={this.state.todos} />

        <ReactCSSTransitionGroup
          className="section todo-list"
          transitionName="slide"
          transitionEnter={true}
          transitionLeave={true}
          transitionAppear={true}
          transitionAppearTimeout={1000}
          transitionEnterTimeout={1000}
          transitionLeaveTimeout={1000} >

          {
            this.state.todos.map((todo) => {
              return <Todo key={todo.guid}
                           id={todo.id}
                           title={todo.title}
                           completed={todo.completed}
                           onStatusCnahge={this.handleStatusChange}
                           onDelete={this.handleDelete}
                           onEdit={this.handleEdit} />
            })
          }

        </ReactCSSTransitionGroup>

        <Form onAdd={this.handleAdd} />

      </div>
    );
  }
}

App.propsTypes = {
  title: React.PropTypes.string.isRequired,
  initialData: React.PropTypes.arrayOf(React.PropTypes.shape({
    id: React.PropTypes.number.isRequired,
    title: React.PropTypes.string.isRequired,
    completed: React.PropTypes.bool.isRequired
  })).isRequired
};

const mapStateToProps = (state) => {
  return {
    todos: state.todos
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    onInitialTodos: () => dispatch(Actions.initialTodos()),

    onAddTodo: (payload) => dispatch(Actions.addTodo(payload)),

    onUpdateTodo: (id, payload) => dispatch(Actions.updateTodo(id, payload)),

    onPatchTodo: (id) => dispatch(Actions.patchTodo(id)),

    onDeleteTodo: (id) => dispatch(Actions.deleteTodo(id))
  };
};

App = connect(mapStateToProps, mapDispatchToProps)(App);

ReactDOM.render(
  <Provider store={store}>
    <App title="React Todo" />
  </Provider>
  , document.querySelector('#root'));
