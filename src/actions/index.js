import axios from 'axios';

import {
  INITITAL_TODOS,
  ADD_TODO,
  UPDATE_TODO,
  PATCH_TODO,
  DELETE_TODO
 } from '../constants/actions.constant';

const errorHandler = (err) => {
  console.log(err);
};

const Actions = {
  initialTodos: () => {
    return dispatch => {
      fetch('/api/todos')
        .then((res) => {
          return res.json();
        })
        .then((todos) => {
          dispatch({
            type: INITITAL_TODOS,
            payload: todos
          });
        })
        .catch((err) => {
          console.log(err);
        });
    };

  },

  addTodo: (payload) => {
    return dispatch => {
      axios.post('/api/todos', payload)
        .then((res) => {
          return res.data;
        })
        .then((todo) => {
          dispatch({
            type: ADD_TODO,
            payload: todo
          });
        })
        .catch(errorHandler);
    };
  },

  updateTodo: (id, payload) => {
    return dispatch => {
      axios.put(`/api/todos/${id}`, payload)
        .then((res) => {
          dispatch({
            type: UPDATE_TODO,
            payload: res.data
          });
        })
        .catch(errorHandler);
    };
  },

  patchTodo: (id) => {
    return dispatch => {
      axios.patch(`/api/todos/${id}`)
        .then((res) => {
          dispatch({
            type: PATCH_TODO,
            payload: res.data
          });
        })
        .catch(errorHandler);
    };
  },

  deleteTodo: (id) => {
    return dispatch => {
      axios.delete(`/api/todos/${id}`)
        .then(() => {
          dispatch({
            type: DELETE_TODO,
            payload: { id }
          });
        })
        .catch(errorHandler);
    };
  }
};

export default Actions;
