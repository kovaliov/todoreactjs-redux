import React from 'react';

const Button = React.createClass({
  propTypes: {
    // variables
    icon: React.PropTypes.string,
    className: React.PropTypes.string,
    children: React.PropTypes.any,

    // functions
    onClick: React.PropTypes.func
  },

  getInitialState() {
    return {
      className: this.props.className,
      icon: this.props.icon
    };
  },

  render() {
    return (
      <button className={this.props.className}
              onClick={this.props.onClick}
              {...this.props}>

          {
            this.props.icon ?
              <i className="material-icons">{this.props.icon}</i> :
                this.props.children
          }

      </button>
    );
  }
});

export default Button;
