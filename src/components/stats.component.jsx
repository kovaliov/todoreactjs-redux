import React from 'react';

function Stats(props) {
  const total = props.todos.length;
  const completed = props.todos.filter(todo => todo.completed).length;
  const notCompleted = total - completed;

  const rowsJSXTemplates = [
    <tr>
      <th>Total task:</th>
      <td>{total}</td>
    </tr>,

    <tr>
      <th>Completed:</th>
      <td>{completed}</td>
    </tr>,

    <tr>
      <th>Left:</th>
      <td>{notCompleted}</td>
    </tr>
  ];

  return (
    <table className="stats">
      <tbody>
        <tr>
          <th>Total task:</th>
          <td>{total}</td>
        </tr>

        <tr>
          <th>Completed:</th>
          <td>{completed}</td>
        </tr>

        <tr>
          <th>Left:</th>
          <td>{notCompleted}</td>
        </tr>
      </tbody>
    </table>
  );
}

Stats.propTypes = {
  // variables
  todos: React.PropTypes.array.isRequired
};

export default Stats;
