import React from 'react';

import Stats from './stats.component';
import Stopwatch from './stopwatch.component';

const Header = props => (
    <div className="header">
      <Stats todos={props.todos} />
      <h1>{props.title}</h1>
      <Stopwatch />
    </div>
  );

Header.propsTypes = {
  // variables
  title: React.PropTypes.string.isRequired,
  todos: React.PropTypes.array.isRequired
};

export default Header;
