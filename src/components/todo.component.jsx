import React from 'react';

import Checkbox from './checkbox.component';
import Button from './button.component';

class  Todo extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      editing: false
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.state.editing) {
      this.refs.title.focus();
      this.refs.title.select();
    }
  }

  handleChange() {
    this.props.onStatusCnahge(this.props.id);
  }

  handleSubmit(event) {
    event.preventDefault();

    const title = this.refs.title.value;

    this.props.onEdit(this.props.id, title);
    this.setState({ editing: false});
  }

  renderDisplay() {
    return (
      <div className={`todo ${this.props.completed ? 'completed' : ''}`}>
        <Checkbox checked={this.props.completed} onChange={this.handleChange} />

        <span className="todo-title">{this.props.title}</span>

        <Button className="edit icon"
                icon={"edit"}
                onClick={() => this.setState({ editing: true })} />
        <Button className="delete icon"
                icon={"delete"}
                onClick={() => this.props.onDelete(this.props.id)} />
      </div>
    );
  }

  renderForm() {
    return (
      <form className="todo-edit-form" onSubmit={this.handleSubmit}>
        <input type="text" ref="title" defaultValue={this.props.title} />

        <Button className="save icon" icon={"save"} type="submit" />
      </form>
    );
  }

  render() {
    const isEditingMode = this.state.editing;

    return isEditingMode ? this.renderForm() : this.renderDisplay();
  }
}

Todo.propsTypes = {
  // variables
  title: React.PropTypes.string.isRequired,
  completed: React.PropTypes.bool.isRequired,

  // functions
  onStatusCnahge: React.PropTypes.func.isRequired,
  onDelete: React.PropTypes.func.isRequired,
  onEdit: React.PropTypes.func.isRequired
};

Todo.defaultProps = {
  completed: false
};

export default Todo;
