import {
  INITITAL_TODOS,
  ADD_TODO,
  UPDATE_TODO,
  PATCH_TODO,
  DELETE_TODO
 } from '../constants/actions.constant';

const todos = (state = {}, action) => {
  switch (action.type) {
    case INITITAL_TODOS: {
      return [...state, ...action.payload];
    }

    case ADD_TODO: {
      return [...state, action.payload];
    }

    case UPDATE_TODO: {
      const index = action.payload.id;
      const todos = state.map(todo => (todo.id === index) ? action.payload : todo);

      return [...todos, action.payload];
    }

    case PATCH_TODO: {
      const index = action.payload.id;
      // const todos = state.map(todo => (todo.id === index) ? action.payload : todo);
      const todos = state.map((todo) => {
        if (todo.id === index) {
          return action.payload;
        }

        return todo;
      });

      return [...todos];
    }

    case DELETE_TODO: {
      const index = action.payload.id;
      const todos = state.filter(todo => todo.id !== index);

      return [...todos];
    }

    default: {
      return [...state];
    }
  }
};

export default todos;
